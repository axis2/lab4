#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "errhandle.h"


#define forever for(;;)


void *printForever(void *_) {
    unsigned line_no = 0;

    forever {
        printf("line #%u\n", line_no++);
        pthread_testcancel();
    }

    return NULL;
}

#define WAIT_TIME_S                 2u

int main() {
    int errcode;

    pthread_t thread;
    errcode = pthread_create(&thread, NULL, printForever, NULL);
    if (NO_ERROR != errcode) {
        errorfln("failed to start a thread: %s", strerror(errcode));
        return EXIT_FAILURE;
    }

    unsigned int time_left = WAIT_TIME_S;
    do {
        time_left = sleep(time_left);
    } while (time_left > 0);

    errcode = pthread_cancel(thread);
    if (NO_ERROR != errcode) {
        errorfln("failed to cancel child thread: %s", strerror(errcode));
        return EXIT_FAILURE;
    }
    errcode = pthread_join(thread, NULL);
    if (NO_ERROR != errcode) {
        errorfln("failed to join child thread: %s", strerror(errcode));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
